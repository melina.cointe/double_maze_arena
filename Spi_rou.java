// Spirou 2021 version: for spirals that are entirely on one image
// Vincent Calcagno
 
import ij.*;
import ij.process.*; 
import ij.gui.*;
import ij.util.Tools.*;
import ij.plugin.filter.*;
import java.awt.*;
import java.io.*;
import ij.plugin.frame.*;
import ij.measure.ResultsTable;
import java.util.Date;
import java.text.SimpleDateFormat;



public class Spi_rou implements PlugInFilter {
	
int imWidth, imHeight;
String imName;
private static int flags =   	DOES_8G;


	public int setup(String arg, ImagePlus imp)  {
		// plugin initialization
		if (imp == null)
		{
			IJ.log("Image is needed.");
			return DONE;
		}
		// store image facts
		this.imWidth = imp.getWidth();
		this.imHeight = imp.getHeight();
		this.imName = imp.getShortTitle();

		return flags;
	}
	
	public static int findClosest(float[] dis,  int[] ind, int nb) {
		// finds the closest particle, ignoring particles already scanned
		float mimi = Float.MAX_VALUE;
		int cloclo = 0;
		for (int i = nb; i < dis.length; i++) {
			if (dis[ind[i]] < mimi) {
				mimi = dis[ind[i]];
				cloclo = i;
			}
		}
		return(cloclo);
	}
	
	int[] getLeftRight(float[] numbers) {
			int min= 0;
			int max=0;
			for (int i=0; i<numbers.length; i++) {
				if (numbers[i]>numbers[max]) max=i;
				if (numbers[i]<numbers[min]) min=i;
				}
			return new int[]{min, max};
		}


	
	
	public void run(ImageProcessor imp) {
		IJ.log("start");
		ResultsTable riri = Analyzer.getResultsTable();
		// get the position of particle centers of masses
		float[] lesx = riri.getColumn(ResultsTable.X_CENTER_OF_MASS);
		float[] lesy = riri.getColumn(ResultsTable.Y_CENTER_OF_MASS);
		int nbpart = lesx.length;
//IJ.log(String.valueOf(nbpart));
		

		// locate initial particle
		// the arrays are by default sorted in increasing order on the y position (thus lesy is sorted ascendingly, lesx is unsorted)
		// this means the first particle is on top of the image (smallest y) and is thus the first particle in the tables
		// this is not true for the 2021 spirals.
		// the top most particle will be on the right or left arm, not too far from center
		// thus we first locate the central of the spiral
		// find left and right boundaries:
		int[] boundies = getLeftRight(lesx);
		// compute coordinates of mean point
		float meanX = (lesx[boundies[0]]+lesx[boundies[1]])/2.0f;
		float meanY = (lesy[boundies[0]]+lesy[boundies[1]])/2.0f;
		// find particle closest to mean point: it is our central particle
		int meanI = 0;
		float didi = Float.MAX_VALUE;
		for (int i=0; i<nbpart; i++) {
			float dd = (float)Math.hypot((double)(meanX-lesx[i]), (double)(meanY-lesy[i]));
			if (dd<didi) {
				didi = dd;
				meanI = i;
			}
		}
		// ok the central particle is meanI.
		// we'll thus start from this one.
IJ.log("Central point is at " + String.valueOf(lesx[meanI]) + " " + String.valueOf(lesy[meanI]));
		
		// We first identify those particles that are on the right side, and those on the left sides
		// i.e. the two branches of the spiral
		int[] indicesR = new int[nbpart];
		int[] indicesL = new int[nbpart];
		int nbpartL = 0;
		int nbpartR=0;
		for (int i=0; i<nbpart; i++) {
			if(lesx[i]>lesx[meanI]) {
				indicesR[nbpartR] = i; nbpartR++;}
				else if(lesx[i]<lesx[meanI]) {
				indicesL[nbpartL] = i; nbpartL++;
				}
		}
		// We then treat both branches individually, one after the other
		
IJ.log("Right branch has " + String.valueOf(nbpartR) + " particles." );
IJ.log("Left branch has " + String.valueOf(nbpartL) + " particles." );


		// First, the right branch (positive positions)
		// we will sort the array of indices from first to last in the chain

IJ.log("Treating right branch..." );

		// compute the matrix of pairwise distances (this is big but we will use it quite repeatedly)
		float[][] distou = new float[nbpartR][nbpartR];
		for (int i=0; i<(nbpartR); i++) for (int j=i+1; j< (nbpartR) ; j++) {
			distou[i][j] = (float)Math.hypot((double)(lesx[indicesR[j]]-lesx[indicesR[i]]), (double)(lesy[indicesR[j]]-lesy[indicesR[i]]));
			// fill-in symmetric part
			distou[j][i] = distou[i][j];
		}
		

		// find particle closest to central: it is our first particle
		int fifi = 0;
		didi = Float.MAX_VALUE;
		for (int i=0; i<nbpartR; i++) {
			float dd = (float)Math.hypot((double)(lesx[meanI]-lesx[indicesR[i]]), (double)(lesy[meanI]-lesy[indicesR[i]]));
			if (dd<didi) {
				didi = dd;
				fifi = i;
			}
		}
		// ok the first particle is fifi.
IJ.log("First right particle is at " +   String.valueOf(lesx[indicesR[fifi]]) + " " + String.valueOf(lesy[indicesR[fifi]]));
		// create indice table
		int[] indidiR = new int[nbpartR];
		for (int i=0; i<nbpartR; i++) indidiR[i]=i;
		// reorder accordingly
		int firsty = indidiR[fifi];
		indidiR[fifi] = indidiR[0];
 		indidiR[0] = firsty;
		// this is the current number of sorted particles in the chain
		int nbpR = 1;
		// this will be the cumulated distance from central particle
		float[] distilinR = new float[nbpartR];
		distilinR[0] = didi;
IJ.log("It is at a distance of " +  String.valueOf(didi));

		// Now we start from the first particle and iteratively look for the closest particle until we have completed the chain
		int currpart = fifi;
		while (nbpR<nbpartR) {
			// find closest particle from current particle, among particles that are still to be sorted
			int closy = Spi_rou.findClosest(distou[currpart], indidiR, nbpR);
IJ.log(String.valueOf(indicesR[currpart]) + " --- " + String.valueOf(indicesR[indidiR[closy]]));
			// add to the chain: update nbp and indices
			// we swap the two values in indices
			int neopart = indidiR[closy];
			indidiR[closy] = indidiR[nbpR];
			indidiR[nbpR] = neopart;
			// update distilin array
			distilinR[nbpR] = distilinR[nbpR-1] + distou[currpart][neopart];
IJ.log(String.valueOf(indicesR[currpart]) + " --- " + String.valueOf(indicesR[indidiR[closy]])+" d= " + String.valueOf(distilinR[nbpR]));

			// we have one more particle in the chain
			currpart = neopart;
			nbpR++;
		}
		// at this stage array indices contains particles sorted from the first to the last along the chain (up to branching points in the skeleton)
//for (int i =0; i<nbpart; i++) IJ.log(String.valueOf(distilin[i]));
IJ.log("Right branch ends at " + String.valueOf(lesx[indicesR[indidiR[nbpR-1]]]) + "  " + String.valueOf(lesy[indicesR[indidiR[nbpR-1]]]));

		
		// Second, the left branch (negative positions)
		// we will sort the array of indices from first to last in the chain
		// this is the current number of sorted particles in the chain
IJ.log("Treating left branch..." );

		// compute the matrix of pairwise distances (this is big but we will use it quite repeatedly)
		distou = new float[nbpartL][nbpartL];
		for (int i=0; i<(nbpartL); i++) for (int j=i+1; j< (nbpartL) ; j++) {
			distou[i][j] = (float)Math.hypot((double)(lesx[indicesL[j]]-lesx[indicesL[i]]), (double)(lesy[indicesL[j]]-lesy[indicesL[i]]));
			// fill-in symmetric part
			distou[j][i] = distou[i][j];
		}
		

		// find particle closest to central: it is our first particle
		fifi = 0;
		didi = Float.MAX_VALUE;
		for (int i=0; i<nbpartL; i++) {
			float dd = (float)Math.hypot((double)(lesx[meanI]-lesx[indicesL[i]]), (double)(lesy[meanI]-lesy[indicesL[i]]));
			if (dd<didi) {
				didi = dd;
				fifi = i;
			}
		}
		// ok the first particle is fifi.
IJ.log("First left particle is at " +   String.valueOf(lesx[indicesL[fifi]]) + " " + String.valueOf(lesy[indicesL[fifi]]));
		// create indice table
		int[] indidiL = new int[nbpartL];
		for (int i=0; i<nbpartL; i++) indidiL[i]=i;
		// reorder accordingly
		firsty = indidiL[fifi];
		indidiL[fifi] = indidiL[0];
 		indidiL[0] = firsty;
		// this is the current number of sorted particles in the chain
		int nbpL = 1;
		// this will be the cumulated distance from central particle
		float[] distilinL = new float[nbpartL];
		distilinL[0] = didi;
IJ.log("It is at a distance of " +  String.valueOf(didi));

		// Now we start from the first particle and iteratively look for the closest particle until we have completed the chain
		currpart = fifi;
		while (nbpL<nbpartL) {
			// find closest particle from current particle, among particles that are still to be sorted
			int closy = Spi_rou.findClosest(distou[currpart], indidiL, nbpL);
			// add to the chain: update nbp and indices
			// we swap the two values in indices
			int neopart = indidiL[closy];
			indidiL[closy] = indidiL[nbpL];
			indidiL[nbpL] = neopart;
			// update distilin array
			distilinL[nbpL] = distilinL[nbpL-1] + distou[currpart][neopart];
IJ.log(String.valueOf(indicesL[currpart]) + " --- " + String.valueOf(indicesL[indidiL[closy]])+" d= " + String.valueOf(distilinL[nbpL]));
			currpart = neopart;
			// we have one more particle in the chain
			nbpL++;
		}
		// at this stage array indices contains particles sorted from the first to the last along the chain (up to branching points in the skeleton)
//for (int i =0; i<nbpart; i++) IJ.log(String.valueOf(distilin[i]));
IJ.log("Left branch ends at " + String.valueOf(lesx[indicesL[indidiL[nbpL-1]]]) + "  " + String.valueOf(lesy[indicesL[indidiL[nbpL-1]]]));

		
		// OK, we have done the two branches. We just have to pack things up and export.
		
		
		// export positions as a result table. Further calculations will be conducted in R for simplicity.
		riri.reset();
		riri = (ResultsTable)Analyzer.getResultsTable().clone();
		if (riri==null) riri = new ResultsTable();	
		riri.reset();
		// central particle
		riri.incrementCounter();
			riri.addValue("Nb", 0);
			riri.addValue("d", 0);
			riri.addValue("x", lesx[meanI]);
			riri.addValue("y", lesy[meanI]);
		// right branch
		for (int i=0; i<nbpartR; i++) {
			riri.incrementCounter();
			riri.addValue("Nb", i+1);
			riri.addValue("d", distilinR[i]);
			riri.addValue("x", lesx[indicesR[indidiR[i]]]);
			riri.addValue("y", lesy[indicesR[indidiR[i]]]);
		}
		// left branch
		for (int i=0; i<nbpartL; i++) {
			riri.incrementCounter();
			riri.addValue("Nb", nbpartR+i+1);
			riri.addValue("d", distilinL[i]);
			riri.addValue("x", lesx[indicesL[indidiL[i]]]);
			riri.addValue("y", lesy[indicesL[indidiL[i]]]);
		}
		// Display results
		riri.show("Results");
		riri.updateResults();
	
		
		// let's draw it for fun
		ByteProcessor beber= new ByteProcessor(imWidth, imHeight, new byte[imHeight*imWidth], null);
		beber.invert();
		for (int i =0; i<(nbpartR-1); i++) {
			beber.drawOval(Math.round(lesx[indicesR[indidiR[i+1]]]), Math.round(lesy[indicesR[indidiR[i+1]]]),10,10);
			beber.drawLine(Math.round(lesx[indicesR[indidiR[i]]]), Math.round(lesy[indicesR[indidiR[i]]]), Math.round(lesx[indicesR[indidiR[i+1]]]), Math.round(lesy[indicesR[indidiR[i+1]]]));	
		}
		for (int i =0; i<(nbpartL-1); i++) {
			beber.drawOval(Math.round(lesx[indicesL[indidiL[i+1]]]), Math.round(lesy[indicesL[indidiL[i+1]]]),10,10);
			beber.drawLine(Math.round(lesx[indicesL[indidiL[i]]]), Math.round(lesy[indicesL[indidiL[i]]]), Math.round(lesx[indicesL[indidiL[i+1]]]), Math.round(lesy[indicesL[indidiL[i+1]]]));	
		}
		ImagePlus cucu = new ImagePlus("collier", beber);
		cucu.show();
		
		
		
		
	}
	
	

}
